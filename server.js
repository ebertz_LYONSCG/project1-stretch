var express = require('express');
var http = require('http');
var request = require('request');
var humanNames = require('random-human-name');

var app = express();
var server = http.createServer(app);
var io = require('socket.io')(server);
app.use(express.static('static'));
app.set('view engine', 'ejs');


//app.listen(8888);
const API_KEY = 'CC6id4PJW6b8RH6R-ULO6icHV5g';
const endpoint = 'http://www.cleverbot.com/getreply?key=' + API_KEY;
var messages = [];
var clients = [];
var games = [];
var _names = humanNames.RandomNames(1000);
//****** ROUTES
app.get('/', (req,res) => {
	res.render(__dirname + '/static/templates/index.ejs');
});

app.get('/bot', (req,res) => {
	res.render(__dirname + '/static/templates/bot.ejs');
});

app.get('/rtt', (req, res) => {
	res.render(__dirname + '/static/templates/lobby.ejs');
});

app.get('/rtt/:gameId', (req,res) => {
	if (!validateGameId(req.params.gameId) && req.params.gameId.indexOf('.') == -1) {
		console.log('invalid game id!');
		//res.redirect('/rtt');
	}
	console.log("gameId : " +req.params.gameId);
	res.render(__dirname + '/static/templates/pregame.ejs', {gameId : req.params.gameId});
});

app.get('/game', (req,res) => {
	res.render(__dirname + '/static/templates/game.ejs');
});

function Game(host) {
	this.state = 'waiting';
	this.host = host;
	this.gameId = generateRandomGameId();
	this.maxPlayers = 10;
	this.maxMessages = 10;
	this.messagesLeft = this.maxMessages;
	this.timeLimit = 180;
	this.players = [];
	this.responses = 0;
	this.cs = '';
	this.botName = '';
	this.getUsernames = function() {
		var names = [];
		for (i in this.players) {
			if (!this.players[i].username) this.players[i].username = 'UNAMED';
			names.push(this.players[i].username);
		}
		if (this.botName) names.push(this.botName);
		return names;		
	};
	this.getInfoAsJSON = function() {
		return {
			'gameId' : this.gameId,
			'usernames' : this.getUsernames(),
			'ready' : this.getPlayerReadyStatuses(),
			'maxPlayers' : this.maxPlayers,
			'hostname' : this.host.username,
			'maxMessages' : this.maxMessages,
			'timeLimit' : this.timeLimit
		};
	};	
	this.sendInfoToAllPlayers = function() {
		console.log('updating game ' + this.gameId + " for " + this.players.length + " players.");
		var info = this.getInfoAsJSON();
		for (i in this.players)
			this.players[i].emit('pregameUpdate', info);
	};
	this.getPlayerReadyStatuses = function() {
		var readies = [];
		for (i in this.players) 
			readies.push(this.players[i].ready);
		return readies;
	}
	this.isMajorityReady = function() {
		var votes = this.getPlayerReadyStatuses();
		var yesVotes = 0;
		for (i in votes)
			if (votes[i]) yesVotes++;
		return (yesVotes > this.players.length/2 && this.players.length > 1);
	}
	this.startGame = function() {
		setTimeout(() => {
			if (this.responses != this.players.length) {
				leaveGame(this.host);

			}else {
				this.state = 'active';

				function chooseName() {
					return _names[Math.floor(Math.random() * _names.length)].split(" ")[0];
				} 
				this.botName = chooseName();
				this.guesser = this.players[Math.floor(Math.random() * this.players.length)];

				var setStatus = new Promise((resolve, reject) => {
					this.guesser.emit('setGuesser');
					for (i in this.players) {
						this.players[i].username = chooseName();
						if (this.players[i] == this.guesser) this.players[i].username += '(G)';
						this.players[i].emit('setUsername', this.players[i].username);
					}

					this.sendUsernames();
					resolve();
				})
				.then((res) => {			
					for (i in this.players) {
						this.players[i].emit('renderGame');
					}

					for (i in this.players) {
						var m = this.players[i] == this.guesser ? 'I am the guesser!' : 'Am I the bot?';
						this.emitMessage({username : this.players[i].username, message : m,  time : getCurrentTime()});
					}
					this.emitMessage({username : this.botName, message : 'Am I the bot?', time : getCurrentTime()});				

				}
				,(rej) => {
					console.log('oops')
				});


			}
		},3000);

	}
	this.emitMessage = function(message) {
		console.log('sending message to all players...');
		for (i in this.players) {
			this.players[i].emit('message', {username : message.username, message : message.message, time : message.time});
		}
	}
	this.sendUsernames = function() {
		for (i in this.players)
			this.players[i].emit('setUsernames', {'usernames' : this.getUsernames()});
	}

}


//***** SERVER RESPONSES
io.on('connection', function(client) {
	//console.log("client connected.");
	clients.push(client);
	client.hosting = false;
	client.ingame = false;
	client.username = '';
	client.gameId = '';
	client.ready = false;
	client.mode = '';
	client.isGuesser = false;
	client.resetState = function() {
		this.hosting = false;
		this.ingame = false;
		this.gameId = '';
		this.ready = false;
		this.mode = 'search';
		this.isGuesser = false;
	};
	
	//ALL PAGES
	client.on('join', function(mode) {
		client.mode = mode;
		if (client.mode == 'search') {
			if (!client.username) {
				client.emit('requestUsername');
			}
			client.emit('displayGames', exportGameInfo());
		}

	});
	client.on('disconnect', function() {
		console.log('disconncet mode = ' + client.mode);
		if (client.mode == 'search') {
			leaveGame(client);
		}
	});
	//CHAT MODE
	client.on('joined', function(name) {
		for (i in clients){
			if (clients[i].mode == 'default')
				clients[i].emit('server_message', 'User <b>' + name + '</b> joined the chat!');
		}
	});

	client.on('message', function(message) {
		messages.push(message);
		for(i in clients) {
			if (clients[i].mode != 'bot') clients[i].emit('message', message);
		}
	});
	//BOT MODE
	client.on('message_bot', function(message) {
		client.emit('message', message);
		var url = endpoint + '&input=' + message.message;
		if (message.cs) url += "&cs=" + message.cs;
		request(url, function(error, response, body) {
			if(error) console.log(error);
			var data = JSON.parse(body);
			client.emit('set_cs', data.cs);
			client.emit('message', {username : 'ROBOT', message : data.output, time : getCurrentTime()});
		});
	});
	//PREGAME MODE
	client.on('joinGame', function(gameId) {
		var game = findOpenGame(gameId);
		if (game) {
			if (client.ingame) {
				leaveGame(client);
			}
			if (game.players.length == 0) game.host = client;
			game.players.push(client);
			client.ingame = true;
			client.gameId = gameId;
			client.emit('refresh');
			game.sendInfoToAllPlayers();
		}
	});
	client.on('getGameInfo', function(gameId) {
		var game = findGame(gameId);
		if (!game) return;
		game.exportGameInfo();
	});

	client.on('startGame', function() {
		console.log("STARTGAME, " + Date.now());
		var game = findGame(client.gameId);
		if (game) {
			if (game.responses == 0) game.startGame();
			game.responses++;

			console.log(game.responses);
		}
	});

	//SEARCH MODE / LOBBY MODE
	client.on('createGame', function(data) {
		if (client.hosting) return;
		if (client.ingame) return;
		var game = new Game(client);
		if (games.length < 20) games.push(game);
		client.hosting = true;
		client.gameId = game.gameId;
		client.emit('sendGameId', game.gameId);
		console.log(getGameIds());
		
		for (i in clients) {
			if (clients[i].mode == 'search')
				client.emit('displayGames', exportGameInfo());
		}
	});

	client.on('leaveGame', function() {
		leaveGame(client);
	});

	client.on('setUsername', function(name) {
		client.username = name;
	});

	client.on('changeReadyStatus', function () {
		client.ready = ! client.ready;

		var game = findGame(client.gameId);
		if (game) {
			game.sendInfoToAllPlayers();			
			if (game.isMajorityReady() && game.state == 'waiting') {
				game.state = 'ready';
				for (i in game.players) {
					console.log("Starting countdown " + i + ", " + Date.now());
					game.players[i].emit('startGameCountdown');
				}
			}
			console.log('2:' + game.isMajorityReady() + ": " + game.state);
			if ((!game.isMajorityReady()) && game.state == 'ready') {
				console.log('COUNTDOWN ABORTED');
				game.state = 'waiting';
				for (i in game.players)
					game.players[i].emit('abortCountdown');
				game.sendInfoToAllPlayers();
			}
			console.log("player statuses: " + game.getPlayerReadyStatuses() + " game status: " + game.state);
		}
	});

	client.on('findGame', function(id) {
		var logString = "Finding game";
		if(id) logString += "with ID " + id;
		logString += "\n Existing games: " + getGameIds();
		console.log(logString);

		var game = findOpenGame(id);
		if (game && game.host != client){
			console.log("game found: " + game.gameId);
			client.emit('sendGameId', game.gameId);
		}
	});

	client.on('refreshLobby', function() {
		client.emit('displayGames', exportGameInfo());
	});

	//INGAME
	client.on('gameMessage', function(message) {

		var game = findGame(client.gameId);
		if (game) {
			game.emitMessage(message);
			console.log('message from ' + message.username + ", guesser is " + game.guesser.username);
			if (message.username == game.guesser.username) {
				game.messagesLeft--;
				for (i in game.players) game.players[i].emit('updateGameState', {'messagesLeft' : game.messagesLeft});
				console.log('guesser sending a message...');
				if (Math.floor(Math.random() * 100) < 20) {
					console.log('bot decided not to respond');
					return;
				}
				var url = endpoint + '&input=' + message.message;
				if (message.cs) url += "&cs=" + message.cs;
				request(url, function(error, response, body) {
					if(error) console.log(error);
					var data = JSON.parse(body);
					setTimeout(() => {
						client.emit('set_cs', data.cs);
						for (i in game.players)
							game.players[i].emit('message', {username : game.botName , message : data.output, time : getCurrentTime() });
					}, (Math.floor(Math.random() * 4000) + 2000));
				});
				if (game.messagesLeft < 1 ) game.guesser.emit('outOfMessages');
			}
		}
	});
	client.on('guess', function(guess) {
		var game = findGame(client.gameId);
		if (game) {
			var data = {'guess' : guess, 'bot' : game.botName};
			var guesserResult = (guess == game.botName) ? "win" : "lose";
			var foolerResult = (guess == game.botName) ? "lose" : "win"; 

			for (i in game.players) {
				var player = game.players[i];
				if (player == client) player.emit(guesserResult, data);
				else player.emit(foolerResult, data);
				leaveGame(player);
			}
			
		}
	});
});

//*******HELPER FUNCTIONS
function getCurrentTime() {
	function format(str) {
		if (parseInt(str) < 10)
			return "0" + str;
		return str;
	}
	var date = new Date();
	return date.getHours() + ":" + format(date.getMinutes()) + ":" + format(date.getSeconds());
}

function generateRandomGameId() {
	var text = '';
	var charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (let i = 0; i < 6; i++)
		text += charSet.charAt(Math.floor(Math.random() * charSet.length));
	return text;
}

function validateGameId(id) {
	for (i in games) if (games[i].gameId == id) return true;
	return false;
}

function getGameIds() {
	var ids = [];
	for (i in games) ids.push(games[i].gameId);
	return ids;
}

function findOpenGame(id) {
	let game = null;
	for (i in games) {
		let cur = games[i];
		//return first available game if no id specified
		if (id == '') {
			if (cur.state == 'waiting' && cur.players.length < cur.maxPlayers)
				game = cur;
		}
		else if (cur.gameId == id && cur.players.length < cur.maxPlayers)
			game = cur;
	}
	return game;
}
function findGame(id) {
	let game = null;
	for (i in games) {
		let cur = games[i];
		if (cur.gameId == id)
			game = cur;
	}
	if (game == null) console.error('COULD NOT FIND GAME ' + id);
	return game;
}

function exportGameInfo() {
	var data = {}
	for (i in games) {
		let g = games[i];
		data[i] = {
			maxPlayers: g.maxPlayers,
			players: g.players.length,
			gameId : g.gameId,
		}
	}
	return data;
}


//deletes player from a game, if player is the host, deletes game
function leaveGame(client) {
	console.log(client.gameId);
	var game = findGame(client.gameId);
	if (!game) return;
	console.log(game.host.gameId);
	if (client == game.host) {
		deleteGameOnHostDisconnect(game.gameId);
	} else {
		var playerIndex = game.players.indexOf(client);
		game.players.splice(playerIndex,1);
		client.emit('endGame');
		game.sendInfoToAllPlayers();
	}
	client.resetState();
}
function printGames() {
	console.log('--------------------------------------');
	for (i in games) {
		console.log(games[i].gameId + " : " + games[i].players.length);
	}
	console.log('--------------------------------------');
}
function getUsernames(gameId) {
	var names = [];
	for (i in game.players) {
		if (!game.players[i].username) players[i].username = 'UNAMED';
		names.push(game.players[i].username);
	}
	return names;
}

function deleteGameOnHostDisconnect(gameId) {
	var game = findGame(gameId);
	if (!game) return;
	for (i in game.players) {
		game.players[i].emit('endGame');
		game.players[i].ingame = false;
		game.players[i].gameId = '';
		game.host.hosting = false;
	}
	game.host.hosting = false;
	games.splice(games.indexOf(game), 1);	


}
server.listen(8888);
//setInterval(printGames, 2000);