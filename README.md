Welcome to my project 1 "stretch goals" application! Yes, it uses promises. But that's not all...

This application implements a Node.js server that serves a web application.
The app has a text chat interface, a chatbot interface, and a cool game you should check out below!

Technologies Used:

	Node.js
	Node packages:
		Express - Web Framework for routing URLs
		ejs - templating engine for rendering HTML dynamically
		socket-io - asynchronous event-driven communication between server and clients.
		request - used to submit API queries
		human-names - generates random names for players in game
	Chatbot - Cleverbot API, https://www.cleverbot.com/api/
	
Running the application:

	1. Install Node.js and npm
	2. 'npm install' to automatically install all dependencies
	3. 'node server.js' from base directory to launch local server on port 8888!

###The Chat Page
![Scheme](static/Images/chat_window.png)
This simple chat interface allows users to connect, enter their username, and begin sending and receiving messages
from the other users connected. Clicking the 'Talk to a Bot' button will take the user to a private chatroom with
just themselves and a chatbot using the Cleverbot API! 
###The Game Lobby
![Scheme](static/Images/game_lobby.png)
Users can create a game instance, or join one hosted by another user! Games can be searched for, or joined by
clicking on the game in the left sidebar. After joining an instance, the main panel displays the game settings.
Once the majority of the players click the 'ready' button, the game will begin!
###The Game!
####How to play:
The game is tentatively named "Spot-the-Bot", a.k.a "The Reverse Turing Test".
When the game starts, all players enter into a chat window and are given random usernames.

Roles:

	The ChatBot:
	
		One of the players in the chat is actually a chat bot! The bot will only respond to messages sent by the guesser
		(or may randomly decide not to respond). The bot has a random delay on its responses.
		
	The Guesser:
	
		One human player is randomly assigned the guesser role. It is his job to send messages, and deduce which player
		is the chat bot based on the responses. He has a limited number of messages, and can only send a message every 5 seconds
		to give the players a chance to keep up. At any point the Guesser can make a guess as to the bot's identity, if he is 
		correct, he wins!
		
	The Foolers:
	
		The rest of the human players become the foolers! Their objective is to respond to the guesser's messages and trick him
		into thinking they are the bot. The foolers win if the Guesser makes the wrong guess!
		
![Scheme](static/Images/guesser.png)
This is the guesser's view of the game. 
He can click one of the named buttons at the bottom to make a guess.
The '0' to the immediate right of the send button is the time left before he can send a message.
####Can you guess which player is the bot in the example above? Scoll down for the answer!
#.
#.
#.
#.
#.
#. 
#.
#.
#. 
#.
#.
#. 
#.
#.
#. 
#.
#.
#. 
#.
#.
#. 

###Answer: Brittni!
