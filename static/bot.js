var username;
var socket = io();
var cs = '';

socket.on('connect', function(data) {
	socket.emit('join', 'bot');
	var chat = document.getElementById("chat_window");
	var m =`<div class="container" style='border-bottom:1px solid blue;width:100%;max-width:100%;margin:0;'>
			  <font color="red">You are now talking to a robot!</font>
			</div>`;
	$(chat).append(m);

});

socket.on('message', function(message) {
	var chat = document.getElementById("chat_window");

	var m = `<div class="container" style='border-bottom:1px solid blue;width:100%;max-width:100%;margin:0;'>
			  <div class="row justify-content-md-center">
			    <div class="col col-sm-2" style="align:left;"> <b>${message.username}:</b> </div>
			    <div class="col col-sm-8"> ${message.message}  </div>
			    <div class="col col-sm-2" style="align:right;"> ${message.time} </div>
			  </div>
			</div>`;
	$(chat).append(m);	
	chat.scrollTop = chat.scrollHeight;
});
socket.on('set_cs', function(data) {
	cs = data;
});


function sendToBot() {
	var input = document.getElementById('input');
	var message = input.value;
	var time = getCurrentTime();
	if(input.value) {
		socket.emit('message_bot', {username : username, message : message, time: time, cs: cs});
	}
	input.value = '';
}

function getCurrentTime() {
	function format(str) {
		if (parseInt(str) < 10)
			return "0" + str;
		return str;
	}
	var date = new Date();
	return date.getHours() + ":" + format(date.getMinutes()) + ":" + format(date.getSeconds());
}

$(document).ready(function() {
	username = 'You';
	$("#input").on('keyup', function (e) {
		if (e.keyCode == 13) sendToBot();
	
	});
});