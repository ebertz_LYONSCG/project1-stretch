
console.log('script loaded');
$(document).ready( function() {
	var socket = io();
	var cs = '';
	socket.on('message', function(message) {
		console.log('got a message');
		var chat = document.getElementById("chat_window");

		var m = `<div class="container" style='border-bottom:1px solid blue;width:100%;max-width:100%;margin:0;'>
				  <div class="row justify-content-md-center">
				    <div class="col col-sm-2" style="align:left;"> <b>${message.username}:</b> </div>
				    <div class="col col-sm-8"> ${message.message}  </div>
				    <div class="col col-sm-2" style="align:right;"> ${message.time} </div>
				  </div>
				</div>`
		$(chat).append(m);	
		chat.scrollTop = chat.scrollHeight;
	});
	socket.on('set_cs', function(data) {
		cs = data;
	});
});
