var socket = io();
var games = [];
var currGameId = '';
var readyStatus = false;
var time;
var username = '';
var cs = '';
var guesser = false;
var delay = 0;
usernames = [];
socket.on('connect', function(data) {
	socket.emit('join', 'search');
});

socket.on('sendGameId', function(id) {
	socket.emit('joinGame', id);
	$('#display').attr('src', '/rtt/' + id);

});

socket.on('displayGames', function(games) {
	var div = $("#displayGames");
	div.html("<tr><th>Game ID</th><th>Players</th></tr>");
	for (i in games) {
		let g = games[i];
		if (g.gameId == currGameId) {
			div.append(`<tr onclick="joinGameById('${g.gameId}')" style="background-color:#ff6699;"><td>${g.gameId}</td><td>${g.players}/${g.maxPlayers}</td></tr>`);
		} else {
			div.append(`<tr onclick="joinGameById('${g.gameId}')"><td>${g.gameId}</td><td>${g.players}/${g.maxPlayers}</td></tr>`);
		}	
	}

});

socket.on('setUsername', function(name) {
	username = name;
});
socket.on('set_cs', function(data) {
	cs = data;
});
socket.on('setGuesser', function(name) {
	guesser = true;

});
socket.on('setUsernames', function(data) {
	usernames = data.usernames;
});

socket.on('updateGameState', function(state) {
	console.log("update messages left");
	$('#gameInfo').html(`Guesser Has ${state.messagesLeft} Messages Left`);
});

socket.on('pregameUpdate', function(game) {
	currGameId = game.gameId;
	//$('#display').append(`<div id='gameinfo'></div`);
	var div = $('#display');
	div.html("");
	var players = '';
	for (i in game.usernames) {
		//var star = game.usernames[i] == game.host.username ? "*" : "";
		players += "<font color='blue'> " + game.usernames[i] + "</font>";
		if (i != game.usernames.length - 1) players += ", ";
	}
	div.append(`In Game:  ${players} </div>`);
	div.append(`<div> Players:  (${game.usernames.length}/${game.maxPlayers}) </div>`);
	div.append(`<div> Host:  ${game.hostname} </div>`);
	div.append(`<div> Time Limit:  ${game.timeLimit} seconds </div>`);
	div.append(`<div> Maximum Messages:  ${game.maxMessages} </div>`);
	div.append(`<br><div>Need ${Math.max(Math.floor(game.usernames.length/2) + 1,2)} players ready to start! (majority vote)</div>`);
	div.append(`<button id='ready' onclick='toggleReady()'>Ready?</button>`);
	$('#display').append(`<div id='timer'></div>`);


	var yes = "<span class='glyphicon glyphicon-ok' style='color:green'></span>";
	var no = "<span class='glyphicon glyphicon-remove' style='color:red'></span>";

	for (i in game.ready) {
		div.append(game.ready[i] ? yes: no);
	}


});

socket.on('renderGame', function() {
	//$('#display').html(`<iframe scrolling='no' width='100%' height='500px' src='/game'></iframe>`);
	var amIGuesser = guesser ? " Try to guess the bot!" : " Try to fool the guesser!";
	$('#display').html(`
			<h3 align='center'>${username} <font color='blue'>${amIGuesser}</font></h3>
			<input id="input" type="text" style="margin-left: 10.5%;width:50%">
			<button id="send" onclick="send()">Send</button><span id="timeLeft"></span>
			<div id="chat_window" class="container-fluid" style="height:350px;width:90%;margin:auto;margin-top:20px;overflow-y:scroll;border:1px solid black;padding:0;">		
		</div>`);
	$('#display').append(`<div id='gameInfo'>Guesser has 10 Messages Left</div>`);
	if (guesser) {
		console.log(usernames);
		$('#display').append('<Make a Guess:  ');
		for (i in usernames) {
			if (usernames[i] != username) {
				$('#display').append(`<button onclick="guess(this.innerHTML)">${usernames[i]}</button>`);
			}
		}
	}
	document.getElementById('input').select();
	$("#input").on('keyup', function (e) {
		if (e.keyCode == 13) send();
	});
	

});
socket.on('outOfMessages', function() {
		$("#input").remove();
		$("#send").remove();

});
socket.on('win', function(data) {
	alert(`You WON! The bot was ${data.bot}. The guess was ${data.guess}`);
});
socket.on('lose', function(data) {
	alert(`You LOST! The bot was ${data.bot}. The guess was ${data.guess}`);
});

socket.on('startGameCountdown', function() {
	console.log("COUNTING DOWN");
	countDown();
});
socket.on('abortCountdown', function() {
	console.log("ABORT");
	if (time) clearInterval(time);
	$('#timer').html("");
});
socket.on('message', function(message) {
	var chat = document.getElementById("chat_window");

	if (message.username == username) message.username = `<font color="red">${username}</font>`;
	var m = `<div class="container" style='border-bottom:1px solid blue;width:100%;max-width:100%;margin:0;'>
			  <div class="row justify-content-md-center">
			    <div class="col col-sm-2" style="align:left;"> <b>${message.username}:</b> </div>
			    <div class="col col-sm-8"> ${message.message}  </div>
			    <div class="col col-sm-2" style="align:right;"> ${message.time} </div>
			  </div>
			</div>`
	$(chat).append(m);	
	chat.scrollTop = chat.scrollHeight;
});

socket.on('refresh', function() {
	refresh();
});

socket.on('requestUsername', function() {
	var username = '';
	//username = prompt('Enter your display name: ');
	if (!username) username = 'Anonymous' + Math.floor(Math.random() * 100000);
	socket.emit('setUsername', username);
});

socket.on('endGame', function() {
	$("#display").html("<font color='red'>The game lobby no longer exists.</font>");
	refresh();
});

/*socket.on('showGames', function(_games) {
	games = _games;
});*/

function joinFirstAvailableGame() {
	console.log("joinFirstAvailableGame");
	socket.emit('findGame', '');


}

function joinGameById(id) {

	var gameId = document.getElementById("gameIdInput").value;
	if (id) gameId = id;
	console.log("joinGameById " + gameId);
	socket.emit('findGame', gameId);


}
function countDown() {
	var i = 10;
	if (time) clearInterval(time);
		time = setInterval(function() {

			if (i >= 0) {
				console.log(i);
				$('#timer').html('<h3>Game starting: '+i+'</h3>');
				i--;
			}else {
				console.log('time up!');
				console.log("firing startgame event, " + Date.now());
				socket.emit('startGame');
				clearInterval(time);

			}
		}, 100);
}

function createGame() {
	console.log("createGame");
	socket.emit('createGame');
}
function leaveGame() {
	socket.emit('leaveGame');
}
function refresh() {
	socket.emit('refreshLobby', currGameId);
}
function toggleReady() {
	console.log('toggle');
	var button = $('#ready');
	readyStatus = !readyStatus;

	/*if (readyStatus) {
		button.css('background-color', 'green');
	} else {
		button.css('background-color', 'gray');
	}*/
	socket.emit('changeReadyStatus');
}
function send() {
	var message = document.getElementById('input').value;
	if (!message) return;
	console.log("tie left: " +delay);
	if (guesser) {
		if (delay > 0) return;
		delay = 6;
		console.log('sending....');
		var message = document.getElementById('input').value;
		document.getElementById('input').value = '';
		socket.emit('gameMessage', {'message' : message, 'username' : username, 'time' : getCurrentTime(), 'cs' : cs} );
		time = setInterval(() =>{
			delay--;
			$('#timeLeft').html(delay);
			if (delay < 1) {
				delay = 0;
				clearInterval(time);
			}
		},1000);
	} else {

		document.getElementById('input').value = '';
		socket.emit('gameMessage', {'message' : message, 'username' : username, 'time' : getCurrentTime(), 'cs' : cs} );
	};
	
	

}
function getCurrentTime() {
	function format(str) {
		if (parseInt(str) < 10)
			return "0" + str;
		return str;
	}
	var date = new Date();
	return date.getHours() + ":" + format(date.getMinutes()) + ":" + format(date.getSeconds());
}
function guess(guess) {
	console.log(guess);
	socket.emit('guess', guess);
}

$(document).ready(function() {
	setInterval(refresh, 3000);
});
